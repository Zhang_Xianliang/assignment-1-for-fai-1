import numpy as np


class Sigmoid:
    def __init__(self):
        self.y = None

    def __call__(self, x):
        y = 1 / (1 + np.exp(-x))  # 順伝播計算
        self.y = y
        return y

    def backward(self):
        return self.y * (1 - self.y)  # 逆伝播計算


class ReLU:
    def __init__(self):
        self.x = None

    def __call__(self, x):
        self.x = x
        return np.maximum(0, x)  # 順伝播計算

    def backward(self):
        return self.x > 0  # 逆伝播計算


class Softmax:
    def __init__(self):
        self.y = None

    def __call__(self, x):
        exp_x = np.exp(x - x.max(axis=1, keepdims=True))
        y = exp_x / np.sum(exp_x, axis=1, keepdims=True)
        self.y = y
        return y
