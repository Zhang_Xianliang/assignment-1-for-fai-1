import numpy as np
import matplotlib.pyplot as plt
from sklearn.datasets import fetch_openml
from sklearn.model_selection import train_test_split
from layer_def import *
from MLP_model import MLP
from activation_func import *
from optimizer_func import *
from train_func import *

def main():
    # load data
    X, Y = fetch_openml('mnist_784', version=1, data_home="./data/", return_X_y=True)
    X = X / 255.
    Y = Y.astype("int")
    train_x, test_x, train_y, test_y = train_test_split(X, Y, test_size=0.2, random_state=2)
    train_y = np.eye(10)[train_y].astype(np.int32)
    test_y = np.eye(10)[test_y].astype(np.int32)

    # define model 1
    # Linear(Sigmoid) - Linear(Sigmoid) - Linear(Softmax)
    # opt: SGD lr=0.1

    # index = 1
    # optimizer = SGD(lr=0.1)
    # model = MLP([Linear(784, 1000, Sigmoid, optimizer), Linear(1000, 1000, Sigmoid, optimizer),
    #              Linear(1000, 10, Softmax, optimizer)])
    # train_acc, test_acc = training(model, train_x, train_y, test_x, test_y, 
    #                                id=index, n_epoch=100, batch_size=128)

    # plot(train_acc, test_acc)

    # define model 2
    # Linear(ReLU) - Linear(ReLU) - Linear(Softmax)
    # opt: SGD lr=0.1
    
    # index = 2
    # optimizer = SGD(lr=0.1)
    # model = MLP([Linear(784, 1000, ReLU, optimizer), Linear(1000, 1000, ReLU, optimizer),
    #              Linear(1000, 10, Softmax, optimizer)])
    # train_acc, test_acc = training(model, train_x, train_y, test_x, test_y, 
    #                                id=index, n_epoch=100, batch_size=128)

    # define model 3
    # Linear(Sigmoid) - Linear(Sigmoid) - Linear(Softmax)
    # opt: Momentum lr=0.01

    # index = 3
    # optimizer = Momentum(lr=0.01)
    # model = MLP([Linear(784, 1000, Sigmoid, optimizer), Linear(1000, 1000, Sigmoid, optimizer),
    #              Linear(1000, 10, Softmax, optimizer)])
    # train_acc, test_acc = training(model, train_x, train_y, test_x, test_y, 
    #                                id=index, n_epoch=200, batch_size=128)

    # define model 4
    # Linear(ReLU) - Linear(ReLU) - Linear(Softmax)
    # opt: Momentum lr=0.01

    # index = 4
    # optimizer = Momentum(lr=0.01)
    # model = MLP([Linear(784, 1000, ReLU, optimizer), Linear(1000, 1000, ReLU, optimizer),
    #              Linear(1000, 10, Softmax, optimizer)])
    # train_acc, test_acc = training(model, train_x, train_y, test_x, test_y, 
    #                                id=index, n_epoch=200, batch_size=128)

    # define model 5
    # Linear(Sigmoid) - Linear(Sigmoid) - Linear(Softmax)
    # opt: AdaGrad lr=0.01

    # index = 5
    # optimizer = AdaGrad(lr=0.005)
    # model = MLP([Linear(784, 1000, Sigmoid, optimizer), Linear(1000, 1000, Sigmoid, optimizer),
    #              Linear(1000, 10, Softmax, optimizer)])
    # train_acc, test_acc = training(model, train_x, train_y, test_x, test_y, 
    #                                id=index, n_epoch=200, batch_size=128)

    # define model 6
    # Linear(ReLU) - Linear(ReLU) - Linear(Softmax)
    # opt: AdaGrad lr=0.01

    # index = 6
    # optimizer = AdaGrad(lr=0.01)
    # model = MLP([Linear(784, 1000, ReLU, optimizer), Linear(1000, 1000, ReLU, optimizer),
    #              Linear(1000, 10, Softmax, optimizer)])
    # train_acc, test_acc = training(model, train_x, train_y, test_x, test_y, 
    #                                id=index, n_epoch=50, batch_size=128)


    # define model 7
    # Linear(ReLU) - Linear(ReLU) - Doupout(0.5) - Linear(Softmax)
    # opt: AdaGrad lr=0.01
    # index = 7
    # optimizer = AdaGrad(lr=0.01)
    # model = MLP([Linear(784, 1000, ReLU, optimizer), Linear(1000, 1000, ReLU, optimizer), DropOut(0.5), 
    #              Linear(1000, 10, Softmax, optimizer)])
    # train_acc, test_acc = training(model, train_x, train_y, test_x, test_y, 
    #                                id=index, n_epoch=200, batch_size=128)

    # define model 8
    # Linear(ReLU) - Linear(ReLU) - Doupout(0.25) - Linear(Softmax)
    # opt: AdaGrad lr=0.01

    # index = 8
    # optimizer = AdaGrad(lr=0.01)
    # model = MLP([Linear(784, 1000, ReLU, optimizer), Linear(1000, 1000, ReLU, optimizer), DropOut(0.25), 
    #              Linear(1000, 10, Softmax, optimizer)])
    # train_acc, test_acc = training(model, train_x, train_y, test_x, test_y, 
    #                                id=index, n_epoch=200, batch_size=128)

    # define model 9
    # Linear(ReLU) - Linear(ReLU) - Doupout(0.75) - Linear(Softmax)
    # opt: AdaGrad lr=0.01
    # index = 9
    # optimizer = AdaGrad(lr=0.01)
    # model = MLP([Linear(784, 1000, ReLU, optimizer), Linear(1000, 1000, ReLU, optimizer), DropOut(0.75), 
    #              Linear(1000, 10, Softmax, optimizer)])
    # train_acc, test_acc = training(model, train_x, train_y, test_x, test_y, 
    #                                id=index, n_epoch=200, batch_size=128)

    # define model 10
    # Linear(ReLU) - Linear(ReLU) -  Linear(Softmax)
    # opt: AdaGrad lr=0.01
    # index = 10
    # optimizer = AdaGrad(lr=0.01)
    # model = MLP([Linear(784, 100, ReLU, optimizer), Linear(100, 30, ReLU, optimizer), 
    #              Linear(30, 10, Softmax, optimizer)])
    # train_acc, test_acc = training(model, train_x, train_y, test_x, test_y, 
    #                                id=index, n_epoch=200, batch_size=128)

    # define model 11
    # Linear(ReLU) - DropOut(0.9) - Linear(ReLU) - Doupout(0.9) - Linear(Softmax)
    # opt: AdaGrad lr=0.01
    # index = 11
    # optimizer = AdaGrad(lr=0.01)
    # model = MLP([Linear(784, 100, ReLU, optimizer), DropOut(0.9), Linear(100, 30, ReLU, optimizer), DropOut(0.9), 
    #              Linear(30, 10, Softmax, optimizer)])
    # train_acc, test_acc = training(model, train_x, train_y, test_x, test_y, 
    #                                id=index, n_epoch=200, batch_size=128)

    # define model 12
    # Linear(ReLU) - Doupout(0.5) - Linear(ReLU) - Doupout(0.5) - Linear(Softmax)
    # opt: AdaGrad lr=0.01

    index = 12
    optimizer = AdaGrad(lr=0.01)
    model = MLP([Linear(784, 1000, ReLU, optimizer), DropOut(0.5), Linear(1000, 1000, ReLU, optimizer), DropOut(0.5), 
                 Linear(1000, 10, Softmax, optimizer)])
    train_acc, test_acc = training(model, train_x, train_y, test_x, test_y, 
                                   id=index, n_epoch=200, batch_size=128)


    # define model 13
    # Linear(ReLU) - Linear(ReLU) - Linear(ReLU) -  Linear(Softmax)
    # opt: AdaGrad lr=0.01

    # index = 13
    # optimizer = AdaGrad(lr=0.005)
    # model = MLP([Linear(784, 1000, ReLU, optimizer), DropOut(0.9),  Linear(1000, 500, ReLU, optimizer), DropOut(0.9),
    #              Linear(500, 100, ReLU, optimizer), DropOut(0.9), DropOut(0.5), Linear(100, 10, Softmax, optimizer)])
    # train_acc, test_acc = training(model, train_x, train_y, test_x, test_y, 
    #                                id=index, n_epoch=200, batch_size=128)


if __name__ == '__main__':
    main()
