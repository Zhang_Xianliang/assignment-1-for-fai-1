import numpy as np
from activation_func import *
import copy


class Linear:
    def __init__(self, in_dim, out_dim, activation, optimizer):
        if activation == ReLU:
            # Kaiming initialization
            self.W = np.random.randn(in_dim, out_dim) / np.sqrt(in_dim / 2)  
        else:
            # Xavier initialization
            self.W = np.random.randn(in_dim, out_dim) / np.sqrt(in_dim)
        self.b = np.zeros(out_dim)
        self.in_dim = in_dim
        self.out_dim = out_dim
        self.activation = activation()
        self.optimizer_W = copy.copy(optimizer)
        self.optimizer_b = copy.copy(optimizer)
        self.delta = None
        self.x = None
        self.dW = None
        self.db = None

    def __call__(self, x, train_flag=True):
        # 順伝播計算
        self.x = x
        u = np.dot(x, self.W) + self.b  # self.W, self.b, x を用いて u を計算しよう
        self.z = self.activation(u)
        return self.z

    def backward(self, dout):
        # 誤差計算
        # dout と活性化関数の逆伝播 (self.activation.backward()) を用いて delta を計算しよう
        self.delta = dout * self.activation.backward()
        # self.delta, self.W を用いて 出力 o を計算しよう
        dout = np.dot(self.delta, self.W.T)

        # 勾配計算
        self.dW = np.dot(self.x.T, self.delta)  # dW を計算しよう
        self.db = np.dot(np.ones(self.x.shape[0]), self.delta)  # db を計算しよう

        return dout

    def update_param(self):
        self.optimizer_W.update(self.W, self.dW)
        self.optimizer_b.update(self.b, self.db)


class DropOut:
    def __init__(self, dropout_ratio=0.8):
        self.dropout_ratio = dropout_ratio
        self.mask = None

    def __call__(self, x, is_traing=True):
        if is_traing:
            self.mask = np.random.rand(x.shape[0], x.shape[1]) < self.dropout_ratio
            return x * self.mask / self.dropout_ratio
        else:
            return x

    def backward(self, dout):
        return dout * self.mask

    def update_param(self):
        pass