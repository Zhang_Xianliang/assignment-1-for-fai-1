import numpy as np


class SGD:
    def __init__(self, lr=0.1):
        self.lr = lr

    def update(self, param, grad):
        param -= self.lr * grad


class Momentum:
    def __init__(self, lr=0.1, momentum=0.9):
        self.lr = lr
        self.momentum = momentum
        self.v = None

    def update(self, param, grad):
        if self.v is None:
            self.v = np.zeros_like(param)

        self.v = self.momentum * self.v - self.lr * grad
        param += self.v


class AdaGrad:
    def __init__(self, lr=0.1):
        self.lr = lr
        self.h = None

    def update(self, param, grad):
        if self.h is None:
            self.h = np.zeros_like(param)

        self.h += grad * grad
        param -= self.lr * grad / (np.sqrt(self.h) + 1e-7)
