import numpy as np
import matplotlib.pyplot as plt
from sklearn.datasets import fetch_openml
from sklearn.model_selection import train_test_split
from layer_def import *
from MLP_model import MLP
from activation_func import *
from optimizer_func import *


def training(model, train_x, train_y, test_x, test_y, id = 1, n_epoch=40, batch_size=128):
    train_acc = []
    test_acc = []
    train_n = train_x.shape[0]
    test_n = test_x.shape[0]
    best_acc = 0
    with open('./log/log_for_model_%d.txt' % id, 'a') as f:
            f.write('log_for_model_%d\r\n' % id)
    for epoch in range(n_epoch):
        log = 'epoch %d | ' % epoch
        print(log, end="")

        # 訓練
        sum_loss = 0
        pred_y = []
        perm = np.random.permutation(train_n)

        for i in range(0, train_n, batch_size):
            x = train_x[perm[i: i + batch_size]]
            t = train_y[perm[i: i + batch_size]]
            sum_loss += model.train(x, t) * len(x)
            pred_y.extend(np.argmax(model.y, axis=1))

        loss = sum_loss / train_n

        # accuracy : 予測結果を1-hot表現に変換し，正解との要素積の和を取ることで，正解数を計算できる．
        accuracy = np.sum(np.eye(10)[pred_y] * train_y[perm]) / train_n
        train_acc.append(accuracy)
        log += 'Train loss %.3f, accuracy %.4f | ' % (loss, accuracy)
        print('Train loss %.3f, accuracy %.4f | ' % (loss, accuracy), end="")

        # テスト
        sum_loss = 0
        pred_y = []

        for i in range(0, test_n, batch_size):
            x = test_x[i: i + batch_size]
            t = test_y[i: i + batch_size]

            sum_loss += model.test(x, t) * len(x)
            pred_y.extend(np.argmax(model.y, axis=1))

        loss = sum_loss / test_n
        accuracy = np.sum(np.eye(10)[pred_y] * test_y) / test_n
        if accuracy > best_acc:
            best_acc = accuracy
        test_acc.append(accuracy)
        log += 'Train loss %.3f, accuracy %.4f | ' % (loss, accuracy)
        print('Test loss %.3f, accuracy %.4f' % (loss, accuracy))

        with open('./log/log_for_model_%d.txt' % id, 'a') as f:
            f.write(log + '\r\n')

    print('Best accuracy %.4f' % best_acc)
    with open('./log/log_for_model_%d.txt' % id, 'a') as f:
        f.write('Best accuracy %.4f\r\n' % best_acc)
    return train_acc, test_acc


def plot(train_acc, test_acc):
    x = np.arange(len(train_acc))
    plt.plot(x, train_acc, marker='o', label='train')
    plt.plot(x, test_acc, marker='s', label='test')
    plt.xlabel("epochs")
    plt.ylabel("accuracy")
    plt.ylim(0.8, 1.0)
    plt.legend(loc='lower right')
    plt.show()
