import numpy as np
import layer_def


class MLP:
    def __init__(self, layers):
        self.layers = layers

    def train(self, x, t):
        # 1. 順伝播
        self.y = x
        for layer in self.layers:
            self.y = layer(self.y)  # 順伝播計算を順番に行い， 出力 y を計算しよう

        # 2. 損失関数の計算
        self.loss = np.sum(-t * np.log(self.y + 1e-7)) / len(x)

        # 3. 誤差逆伝播
        # 3.1. 最終層
        # 3.1.1. 最終層の誤差・勾配計算
        batchsize = len(self.layers[-1].x)
        delta = (self.y - t) / batchsize
        self.layers[-1].delta = delta
        self.layers[-1].dW = np.dot(self.layers[-1].x.T, self.layers[-1].delta)
        self.layers[-1].db = np.dot(np.ones(batchsize), self.layers[-1].delta)
        dout = np.dot(self.layers[-1].delta, self.layers[-1].W.T)

        # 3.1.2. 最終層のパラメータ更新
        # use optimizer
        # self.layers[-1].dW を用いて最終層の重みを更新しよう
        # self.layers[-1].W -= lr * self.layers[-1].dW
        # self.layers[-1].db を用いて最終層のバイアスを更新しよう
        # self.layers[-1].b -= lr * self.layers[-1].db
        self.layers[-1].update_param()

        # 3.2. 中間層
        for layer in self.layers[-2::-1]:
            # 3.2.1. 中間層の誤差・勾配計算
            dout = layer.backward(dout)  # 逆伝播計算を順番に実行しよう

            # 3.2.2. パラメータの更新
            # use optimizer
            # layer.W -= lr * layer.dW  # 各層の重みを更新
            # layer.b -= lr * layer.db  # 各層のバイアスを更新
            layer.update_param()

        return self.loss

    def test(self, x, t):
        # 性能をテストデータで調べるために用いる
        # よって，誤差逆伝播は不要
        # 順伝播 (train関数と同様)
        self.y = x
        for layer in self.layers:
            self.y = layer(self.y, False)
        self.loss = np.sum(-t * np.log(self.y + 1e-7)) / len(x)
        return self.loss
