# README #

------

#### Assignment one for 先端人工知能論Ⅰ／Frontier Artificial Intelligence I  

#### Theme: MLP to recognize numbers in MNIST dataset  

#### Name: Zhang Xianliang 張賢亮  

#### Date: 2020/5/12  

Code Link: [bitbucket](https://bitbucket.org/Zhang_Xianliang/assignment-1-for-fai-1/src/master/)

> Or you can clone codes by using command:   
> 
> git clone https://Zhang_Xianliang@bitbucket.org/Zhang_Xianliang/assignment-1-for-fai-1.git

***Best test accuracy: 98.73%***

>try Dropout, Xavier / Kaiming initialization, Optimizer(SGD, Momentun, AdaGrad) and so on

------

### File/Folder Infomation

| Name               | Description                             |
| ------------------ | --------------------------------------- |
| ./data/            | data for training and test              |
| ./log/             | logs of each model during training      |
| README.md          | README                                  |
| activation_func.py | definitions of activation functions     |
| optimizer_func.py  | definitions of optimizer functions      |
| MLP_model.py       | definition of the Multilayer Perceptron |
| layer_def.py       | definition of one Linear layer          |
| train_func.py      | definitions of train and test function  |
| mlp_MNIST.py       | main function                           |


### Results of each model

---

| index | accuracy |
| :---: | :------: |
|   1   |  96.88%  |
|   2   |  98.31%  |
|   3   |  97.86%  |
|   4   |  98.25%  |
|   5   |  98.02%  |
|   6   |  98.40%  |
|   7   |  98.51%  |
|   8   |  98.60%  |
|   9   |  98.49%  |
|  10   |  97.97%  |
|  11   |  98.06%  |
|  12   |  98.73%  |
|  13   |  98.61%  |
| best  |  98.73%  |

---

* Model 1:  

    Structure: Linear(784, 1000, Sigmoid) -> Linear(1000, 1000, Sigmoid) -> Linear(1000, 10, Softmax)  

    Optimizer: SGD;  lr=0.1  

    ***best accuracy = 96.88%***  

* Model 2:  

    Structure: Linear(784, 1000, ReLU) -> Linear(1000, 1000, ReLU) -> Linear(1000, 10, Softmax)  

    Optimizer: SGD;  lr=0.1  

    ***best accuracy = 98.31%***  

* Model 3:  

    Structure: Linear(784, 1000, Sigmoid) -> Linear(1000, 1000, Sigmoid) -> Linear(1000, 10, Softmax)  

    Optimizer: Momentum; lr=0.01  

    ***best accuracy = 97.86%***  

* Model 4:  

    Structure: Linear(784, 1000, ReLU) -> Linear(1000, 1000, ReLU) -> Linear(1000, 10, Softmax)  

    Optimizer: Momentum; lr=0.01  

    ***best accuracy = 98.25%***  


* Model 5:  

    Structure: Linear(784, 1000, Sigmoid) -> Linear(1000, 1000, Sigmoid) -> Linear(1000, 10, Softmax)  

    Optimizer: AdaGrad; lr=0.005  

    ***best accuracy = 98.02%***  


* Model 6:  

    Structure: Linear(784, 1000, ReLU) -> Linear(1000, 1000, ReLU) -> Linear(1000, 10, Softmax)  

    Optimizer: AdaGrad; lr=0.01  

    ***best accuracy = 98.40%***  


* Model 7:  

    Structure: Linear(784, 1000, ReLU) -> Linear(1000, 1000, ReLU) -> DropOut(0.5) -> Linear(1000, 10, Softmax)  

    Optimizer: AdaGrad; lr=0.01  

    ***best accuracy = 98.51%***  


* Model 8:  

    Structure: Linear(784, 1000, ReLU) -> Linear(1000, 1000, ReLU) -> DropOut(0.25) -> Linear(1000, 10, Softmax)  

    Optimizer: AdaGrad; lr=0.01  

    ***best accuracy = 98.60%***  


* Model 9:  

    Structure: Linear(784, 1000, ReLU) -> Linear(1000, 1000, ReLU) -> DropOut(0.75) -> Linear(1000, 10, Softmax)  

    Optimizer: AdaGrad; lr=0.01  

    ***best accuracy = 98.49%***  


* Model 10:  

    Structure: Linear(784, 100, ReLU) -> Linear(100, 30, ReLU) -> Linear(30, 10, Softmax)  

    Optimizer: AdaGrad; lr=0.01  

    ***best accuracy =  97.97%***  


* Model 11:  

    Structure: Linear(784, 100, ReLU) -> DropOut(0.9) -> Linear(100, 30, ReLU) -> DropOut(0.9)  -> Linear(30, 10, Softmax)  

    Optimizer: AdaGrad; lr=0.01  

    ***best accuracy = 98.06%***  


* Model 12:  

    Structure: Linear(784, 1000, ReLU) -> DropOut(0.5) -> Linear(1000, 1000, ReLU) -> DropOut(0.5) -> Linear(1000, 10, Softmax)  

    Optimizer: AdaGrad; lr=0.01  

    ***<u>best accuracy = 98.73%</u>***  


* Model 13:  

    Structure: Linear(784, 1000, ReLU) -> DropOut(0.5) -> Linear(1000, 500, ReLU) -> DropOut(0.5) -> Linear(500, 100, ReLU) -> DropOut(0.5) -> Linear(100, 10, Softmax)  

    Optimizer: AdaGrad; lr=0.01  

    ***best accuracy =  98.61%***  


## Conclusion

今回実験されたモデルのうち、一番高いaccuracy(98.73%)を達成したモデルのストラクチャはこんな感じです：Linear(784, 1000, ReLU) -> DropOut(0.5) -> Linear(1000, 1000, ReLU) -> DropOut(0.5) -> Linear(1000, 10, Softmax).また、epoch数は200で、mini-batchのサイズは128で、optimizerはAdaGradで、学習率は0.01で、Dropoutのratioは0.5でした.
    
今回の実験によって、いろいろを試して、勉強になりました。例えば、Optomizerを使うと、ネットワークの収束の速度は確実に早くにりましたた。また、Dropoutの手法を用いて、一定の程度で過学習を抑えられますが、accuracyが必ず上がると言えません。更に、隠れ層の数が多ければ良いと限らないことも分かりました。最後に、Dropoutのratioが一定の程度でaccuracyと関係があるので、工夫する必要があることが分かりました.